/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ocl_project;

import ocl_project.DescripteurMot;
import OclCollections.*;

/**
 * @author maxime
 */

public class Indexe {
    
  LinkedListSet<DescripteurMot> associations;
  
   public Indexe()
   {
     associations = new LinkedListSet<>();
   }
    
   public void ajouter(final String mot)
   {
    /**
     * @pre 
     */
      
       assert !mot.isEmpty();
      
      assert 
      (!this.associations.isUnique(new Expression<DescripteurMot, Boolean>(){

           @Override
           public Boolean eval(DescripteurMot t) 
           {
             for(DescripteurMot ch : associations) 
              if(!t.mot.equals(ch.mot))
                return false;
             return true;
           }

      }) 
              ||
          
       this.associations.exists(new Expression<DescripteurMot, Boolean>()
       {

           @Override
           public Boolean eval(DescripteurMot t) 
           {
            for(DescripteurMot ch : associations)
               if(!ch.mot.equals(t.mot))
                  return false;
            return true;
           }

       }));
     /**
      * Fin @pre
      */
         
      
      /**
       * @post
       */
      
      assert
        (!this.associations.any(new Expression<DescripteurMot, Boolean>(){
           @Override
           public Boolean eval(DescripteurMot t) 
		   {
              for(DescripteurMot dm : this.associations)
			    if(!dm.mot.equals(t.mot))
				  return false;
		      return true;
	       }
       }).compte 
        
              ==
              
        this.associations.any(new Expression<DescripteurMot, Boolean>(){
           @Override
           public Boolean eval(DescripteurMot t)
		   {
              for(DescripteurMot dm : this.associations)
			    if(!dm.mot.equals(t.mot))
				  return false;
		      return true;
	       }
       }).compte+1)
        
           ||
               
      associations.symmetricDifference(associations.including(new DescripteurMot(mot,1))).size() == 0;
      
   }
   
    public void supprimerOccurenceBasse(final int seuil)
    {
    
    /**
     * @pre 
     */
        
        assert seuil > 1;
        
     
     /**
      * @post
      */
        
       assert this.associations.intersection(this.associations.reject(new Expression<DescripteurMot, Boolean>()
       {
            @Override
            public Boolean eval(DescripteurMot t) 
            {
              return t.compte <= seuil;
            }
       })).size() == this.associations.size();
    }
}
